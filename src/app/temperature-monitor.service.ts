import { Injectable } from '@angular/core';

@Injectable()
export class TemperatureMonitorService {

  constructor() { }

  // function using es6 arrow function
  recordTemperature = (temperature: number, temperatureList: number[]) => {temperatureList.push(temperature); return temperatureList};
    
  sortNumber = (a, b) => {return a - b};

  // function using es5 javascript
  getCurrentMedian(temperatureList: number[]) { 
    var median: number;
    const sortedList = temperatureList.sort(this.sortNumber);
    
    switch (sortedList.length % 2) {
      case 0:
        median = (sortedList[sortedList.length/2 - 1] + sortedList[sortedList.length/2]) / 2;
        break;
      default:
        median = sortedList[Math.floor(sortedList.length/2)];
    }
    return median;
  }

}

import { Component } from '@angular/core';

import { TemperatureMonitorService } from './temperature-monitor.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  medianTemperature;
  temperatureList: number[] = [];

  constructor(private temperatureService: TemperatureMonitorService) {}

  add(formValue) {
    this.temperatureService.recordTemperature(Number(formValue.temperature), this.temperatureList);
  }

  median() {
    this.medianTemperature = this.temperatureService.getCurrentMedian(this.temperatureList);
  }
}

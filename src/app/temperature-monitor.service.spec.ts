import { TestBed, inject } from '@angular/core/testing';

import { TemperatureMonitorService } from './temperature-monitor.service';

describe('TemperatureMonitorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TemperatureMonitorService]
    });
  });

  it('service should exist', inject([TemperatureMonitorService], (service: TemperatureMonitorService) => {
    expect(service).toBeTruthy();
  }));

  it('recordTemperature push to list', inject([TemperatureMonitorService], (service: TemperatureMonitorService) => {
    expect(service.recordTemperature(1, [])).toEqual([1]);
  }));

  it('recordTemperature push to list in the right order', inject([TemperatureMonitorService], (service: TemperatureMonitorService) => {
    expect(service.recordTemperature(1, [2])).toEqual([2, 1]);
  }));

  it('recordTemperature push negative value to list', inject([TemperatureMonitorService], (service: TemperatureMonitorService) => {
    expect(service.recordTemperature(-1, [2])).toEqual([2, -1]);
  }));

  it('sortNumber compare positives', inject([TemperatureMonitorService], (service: TemperatureMonitorService) => {
    expect(service.sortNumber(1, 2) < 0).toEqual(true);
  }));

  it('sortNumber compare negatives', inject([TemperatureMonitorService], (service: TemperatureMonitorService) => {
    expect(service.sortNumber(-1, -2) > 0).toEqual(true);
  }));

  it('sortNumber compare equals', inject([TemperatureMonitorService], (service: TemperatureMonitorService) => {
    expect(service.sortNumber(-1, -1) === 0).toEqual(true);
  }));

  it('getCurrentMedian one item list', inject([TemperatureMonitorService], (service: TemperatureMonitorService) => {
    expect(service.getCurrentMedian([1])).toEqual(1);
  }));

  it('getCurrentMedian two item list', inject([TemperatureMonitorService], (service: TemperatureMonitorService) => {
    expect(service.getCurrentMedian([1, 2])).toEqual(1.5);
  }));

  it('getCurrentMedian odd number list length', inject([TemperatureMonitorService], (service: TemperatureMonitorService) => {
    expect(service.getCurrentMedian([1, 2, 3])).toEqual(2);
  }));

  it('getCurrentMedian unsorted odd number list length', inject([TemperatureMonitorService], (service: TemperatureMonitorService) => {
    expect(service.getCurrentMedian([2, 3, 1])).toEqual(2);
  }));

  it('getCurrentMedian even number list length', inject([TemperatureMonitorService], (service: TemperatureMonitorService) => {
    expect(service.getCurrentMedian([1, 2, 3, 4])).toEqual(2.5);
  }));

  it('getCurrentMedian unsorted even number list length', inject([TemperatureMonitorService], (service: TemperatureMonitorService) => {
    expect(service.getCurrentMedian([4, 2, 3, 1])).toEqual(2.5);
  }));

  it('getCurrentMedian negative value list', inject([TemperatureMonitorService], (service: TemperatureMonitorService) => {
    expect(service.getCurrentMedian([-4, -2, -3, -1])).toEqual(-2.5);
  }));

  it('getCurrentMedian negative/positive value list', inject([TemperatureMonitorService], (service: TemperatureMonitorService) => {
    expect(service.getCurrentMedian([-2, -3, 1])).toEqual(-2);
  }));

  it('getCurrentMedian readMe list #1', inject([TemperatureMonitorService], (service: TemperatureMonitorService) => {
    expect(service.getCurrentMedian([5, 1, -7, 7, 8, 3])).toEqual(4);
  }));

  it('getCurrentMedian readMe list #2', inject([TemperatureMonitorService], (service: TemperatureMonitorService) => {
    expect(service.getCurrentMedian([5, 1, -7, 7, 8, 3, 9])).toEqual(5);
  }));
});

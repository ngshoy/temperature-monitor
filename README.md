# TEMPERATURE MONITOR EXERCISE - JavaScript/SPA

## Getting Started
1. clone this project using: git clone git@gitlab.com:ngshoy/temperature-monitor.git
2. cd temperature-monitor
3. npm install
4. npm start
5. navigate to: http://localhost:4200

## Version Info
- node 7.1.0
- npm 3.10.9
- angular ^ 2.4.0
- angular cli 1.0.0-rc.0
